const {useState, useEffect } = React

function Header(){
    return(
        <div style={{backgroundColor: "#D5573B", textAlign: "center", padding: "10px", width: "80%", margin: "auto", color: "white" }}>
            <img src="https://www.balldontlie.io/images/cryingjordan.jpeg"></img>
            <h1>NBA Player Stats Finder</h1>
        </div>
    )
}

function Stats(){
    const [infos, setInfo] = useState([])
    const [players, setPlayer] = useState('')
    const [playerID, setPlayerID] = useState(0);
    const [stats, setStats] = useState([])

    useEffect(() => {
        axios.get(`https://www.balldontlie.io/api/v1/season_averages?player_ids[]=${playerID}`)
        .then(res => {
            setStats(res.data.data)
        })
        .catch(err => { console.log(err)})
    }, [playerID])

    const handleSubmit = (e) => {
        e.preventDefault();

        axios.get(`https://www.balldontlie.io/api/v1/players?search=${players}`)
        .then(res => {
            setInfo(res.data.data.slice(0,1))
            setPlayerID(res.data.data.map(x => x.id))
        })
        .catch(err => { console.log(err)})

        alert(`Submitting Name ${players}`)
    }


    console.log(players)
    console.log(playerID)

        return (
            <div style={{display: "flex", justifyContent: "center", marginTop:"50px"}}>
                <div>
                    <div style={{display: "flex", backgroundColor: "grey"}} >
                        <div style={{margin: "20px"}}>
                            <form onSubmit={handleSubmit}>
                                <label for="player">Enter Player's name: </label>
                                <input value={players} onChange={e => setPlayer(e.target.value)}></input>
                                <input type="submit" value="Submit"></input>
                            </form>
                        </div>
                    </div>
                    <div style={{textAlign: "center"}}>
                        {infos.slice(0,1).map( info => (
                            <div key={info.id} style={{margin: "10px", padding: "10px"}}>
                                <h1>{info.first_name} {info.last_name}</h1>
                                <h2>{info.team.full_name}</h2>
                            </div>
                        ))}
                        {stats.slice(0,1).map( stat => (
                            <div key={stat} style={{margin: "10px"}}>
                                <h1 style={{color: "lightblue", backgroundColor: "grey", padding: "5px"}}>2021 Season</h1>
                                <h3>Average Score: {stat.pts}</h3>
                                <h3>Rebounds: {stat.reb}</h3>
                                <h3>Assists: {stat.ast}</h3>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    

}

function Footer(){
    return (
        <div>
        </div>
    )
}


function App(){
    return (
        <div>
            <Header />
            <Stats />
            <Footer />
        </div>
    )
}
    
    
ReactDOM.render(
    <App />,
    document.getElementById('root')
)